import csv
import requests
import os
import json
from urllib.parse import unquote_plus


def parse(path):
    assert os.path.exists(path), f"Missing {path}"

    reader = csv.DictReader(open(path), delimiter=';')

    for x in reader:
        yield x['iiif manifest']


def download(url):
    path = os.path.join("./manifests", unquote_plus(url[url.index('://')+3:]))
    if not path.endswith('.json'):
        path += ".json"
    os.makedirs(os.path.dirname(path), exist_ok=True)

    resp = requests.get(url)
    resp.raise_for_status()

    with open(path, "w") as f:
        json.dump(resp.json(), f, sort_keys=True, indent=4)

    return path


if __name__ == '__main__':
    for manifest in parse('manifests.csv'):
        print(manifest)
        try:
            path = download(manifest)
            print(path)
        except requests.exceptions.HTTPError as e:
            print(f"Download failed: {e}")
